#! /bin/bash

if [ -z "$1" ]; then
	echo -e "\e[91mSpecify argument install or purge\e[0m"
	exit 1
fi


if [ "$1" = "purge" ]; then
	echo -e "\e[91mNot removing git, ifconfig and iwconfig\e[0m"
fi

sudo apt-get update
sudo apt-get install git -y
sudo apt-get install net-tools -y
sudo apt-get $1 aircrack-ng -y
sudo apt-get $1 tshark -y
sudo apt-get $1 reaver -y
sudo apt-get $1 pyrit -y
sudo apt-get $1 hashcat -y
sudo apt-get $1 macchanger -y


if [ "$1" = "install" ]; then
	# hcxdumptool
	git clone https://github.com/ZerBea/hcxdumptool.git
	cd hcxdumptool
	sudo make install
	cd ..
	sudo rm -r hcxdumptool

	# hcxtools
	sudo apt-get install zlibc -y
	sudo apt-get install zlib1g -y
	sudo apt-get install zlib1g-dev -y
	sudo apt-get install pibpcap-dev -y
	sudo apt-get install libcurl4-openssl-dev -y
	sudo apt-get install libssl-dev -y
	sudo apt-get install libpcap-dev -y

	git clone https://github.com/ZerBea/hcxtools.git
	cd hcxtools
	sudo make install
	cd ..
	sudo rm -r hcxtools

	# bully
	git clone https://github.com/aanarchyy/bully
	cd bully/src
	sudo make install
	cd ../..
	sudo rm -r bully


elif [ "$1" = "purge" ]; then
	# hcxdumptool
	sudo rm /usr/local/bin/hcxdumptool

	# hcxtools	
	sudo rm /usr/local/bin/hcxpcaptool
	sudo rm /usr/local/bin/hcxpsktool
	sudo rm /usr/local/bin/hcxhashcattool
	sudo rm /usr/local/bin/hcxhash2cap
	sudo rm /usr/local/bin/wlanhc2hcx
	sudo rm /usr/local/bin/wlanwkp2hcx
	sudo rm /usr/local/bin/wlanhcxinfo
	sudo rm /usr/local/bin/wlanhcx2cap
	sudo rm /usr/local/bin/wlanhcx2essid
	sudo rm /usr/local/bin/wlanhcx2ssid
	sudo rm /usr/local/bin/wlanhcxmnc
	sudo rm /usr/local/bin/wlanhashhcx
	sudo rm /usr/local/bin/wlanhcxcat
	sudo rm /usr/local/bin/wlanpmk2hcx
	sudo rm /usr/local/bin/wlanjohn2hcx
	sudo rm /usr/local/bin/wlancow2hcxpmk
	sudo rm /usr/local/bin/whoismac
	sudo rm /usr/local/bin/wlanhcx2john
	sudo rm /usr/local/bin/wlanhcx2psk
	sudo rm /usr/local/bin/wlancap2wpasec

	# bully
	sudo rm /usr/local/bin/bully

		
fi


sudo apt-get autoremove -y

